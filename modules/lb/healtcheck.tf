resource "google_compute_health_check" "dashboard" {
  name = "salsa-dashboard"

  check_interval_sec = 30

  http_health_check {
    request_path       = "/api/health"
    port_specification = "USE_SERVING_PORT"
  }
}

resource "google_compute_health_check" "logs" {
  name = "salsa-logs"

  check_interval_sec = 30

  http_health_check {
    request_path       = "/api/status"
    port_specification = "USE_SERVING_PORT"
  }
}

resource "google_compute_health_check" "logs_elasticsearch" {
  name = "salsa-logs-elasticsearch"

  check_interval_sec = 30

  http_health_check {
    request_path       = "/health"
    port_specification = "USE_SERVING_PORT"
    proxy_header       = "PROXY_V1"
  }
}
