resource "google_compute_url_map" "default" {
  name = "salsa"

  default_url_redirect {
    host_redirect          = var.domain_gitlab
    path_redirect          = "/"
    strip_query            = true
    redirect_response_code = "TEMPORARY_REDIRECT"
  }

  host_rule {
    hosts        = ["dashboard.${var.domain}"]
    path_matcher = "dashboard"
  }

  host_rule {
    hosts        = ["logs.${var.domain}"]
    path_matcher = "logs"
  }

  path_matcher {
    name            = "dashboard"
    default_service = google_compute_backend_service.dashboard.self_link
  }

  path_matcher {
    name            = "logs"
    default_service = google_compute_backend_service.logs.self_link
  }
}

resource "google_compute_url_map" "http" {
  name = "salsa-http"

  default_url_redirect {
    https_redirect = true
    strip_query    = true
  }
}
