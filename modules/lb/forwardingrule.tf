resource "random_pet" "forwarding_rule" {
  for_each = var.addresses

  keepers = {
    address = each.key
  }
}

resource "google_compute_global_forwarding_rule" "http" {
  for_each = var.addresses

  name = "salsa-http-${random_pet.forwarding_rule[each.key].id}"

  ip_address = each.key
  target     = google_compute_target_http_proxy.default.self_link
  port_range = 80
}

resource "google_compute_global_forwarding_rule" "https" {
  for_each = var.addresses

  name = "salsa-https-${random_pet.forwarding_rule[each.key].id}"

  ip_address = each.key
  target     = google_compute_target_https_proxy.default.self_link
  port_range = 443
}

resource "google_compute_global_forwarding_rule" "ssl_logs_elasticsearch" {
  for_each = var.addresses

  name = "salsa-ssl-logs-elasticsearch-${random_pet.forwarding_rule[each.key].id}"

  ip_address = each.key
  target     = google_compute_target_ssl_proxy.elasticsearch.self_link
  port_range = 9200
}
