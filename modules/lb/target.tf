resource "google_compute_target_http_proxy" "default" {
  name    = "salsa-http"
  url_map = google_compute_url_map.http.self_link
}

resource "google_compute_target_https_proxy" "default" {
  name             = "salsa-https"
  url_map          = google_compute_url_map.default.self_link
  ssl_certificates = [google_compute_ssl_certificate.example.self_link]

  lifecycle {
    ignore_changes = [
      ssl_certificates,
    ]
  }
}

resource "google_compute_target_ssl_proxy" "elasticsearch" {
  name             = "salsa-ssl-logs-elasticsearch"
  backend_service  = google_compute_backend_service.logs_elasticsearch.id
  proxy_header     = "PROXY_V1"
  ssl_certificates = [google_compute_ssl_certificate.example.self_link]

  lifecycle {
    ignore_changes = [
      ssl_certificates,
    ]
  }
}
