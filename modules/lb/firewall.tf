resource "google_compute_firewall" "allow_lb" {
  name    = "default-allow-lb"
  network = "default"

  allow {
    protocol = "tcp"
    ports    = [
      22,
      80,
      443,
      3000,
      5601,
      8080,
      9280,
    ]
  }

  source_ranges = [
    "35.191.0.0/16",
    "130.211.0.0/22",
  ]
}
