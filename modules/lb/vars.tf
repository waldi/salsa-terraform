variable "domain" {
}

variable "domain_gitlab" {
}

variable "iap_brand" {
}

variable "addresses" {
  type = set(string)
}

variable "group_dashboard" {
}

variable "group_logs" {
}
