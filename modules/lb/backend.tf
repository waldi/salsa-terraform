resource "google_compute_backend_service" "dashboard" {
  name        = "salsa-dashboard"
  port_name   = "http"
  protocol    = "HTTP"
  timeout_sec = 10

  health_checks = [google_compute_health_check.dashboard.self_link]

  backend {
    group = var.group_dashboard
  }
}

resource "google_compute_backend_service" "logs" {
  name        = "salsa-logs"
  port_name   = "http"
  protocol    = "HTTP"
  timeout_sec = 10

  health_checks = [google_compute_health_check.logs.self_link]

  backend {
    group = var.group_logs
  }

  iap {
    oauth2_client_id     = google_iap_client.default.client_id
    oauth2_client_secret = google_iap_client.default.secret
  }
}

resource "google_compute_backend_service" "logs_elasticsearch" {
  name        = "salsa-tcp-logs-elasticsearch"
  port_name   = "http-elasticsearch"
  protocol    = "TCP"
  timeout_sec = 10

  health_checks = [google_compute_health_check.logs_elasticsearch.self_link]

  backend {
    group = var.group_logs
  }
}
