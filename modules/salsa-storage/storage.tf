resource "google_storage_bucket" "artifacts" {
  name          = "debian-salsa-${var.env}-gitlab-artifacts"
  project       = var.project
  location      = var.location
  storage_class = var.storage_class

  uniform_bucket_level_access = true

  cors {
    origin = [var.origin]
    method = ["GET", "OPTIONS"]
  }
}

resource "google_storage_bucket" "lfs" {
  name          = "debian-salsa-${var.env}-gitlab-lfs"
  project       = var.project
  location      = var.location
  storage_class = var.storage_class

  uniform_bucket_level_access = true

  cors {
    origin = [var.origin]
    method = ["GET", "OPTIONS"]
  }
}

resource "google_storage_bucket_iam_member" "artifacts" {
  bucket = google_storage_bucket.artifacts.name
  role   = "roles/storage.objectAdmin"
  member = "serviceAccount:${google_service_account.gitlab.email}"
}

resource "google_storage_bucket_iam_member" "lfs" {
  bucket = google_storage_bucket.lfs.name
  role   = "roles/storage.objectAdmin"
  member = "serviceAccount:${google_service_account.gitlab.email}"
}
