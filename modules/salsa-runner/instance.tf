resource "google_compute_instance" "runner" {
  name         = "salsa-${var.env}-runner"
  machine_type = var.instance_type
  zone         = var.zone

  allow_stopping_for_update = true

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-9"
      type  = "pd-ssd"
    }
  }

  network_interface {
    network = "default"

    access_config {
      // Ephemeral IP
    }
  }

  service_account {
    email  = google_service_account.runner.email
    scopes = ["cloud-platform"]
  }
}
