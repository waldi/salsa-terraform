# Service account used by the runner to create instances in the build
# project
resource "google_service_account" "runner" {
  account_id = "runner"
  project    = var.project
}
