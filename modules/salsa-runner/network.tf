data "google_compute_network" "default" {
  name    = "default"
  project = var.project
}

resource "google_compute_network" "build" {
  name                    = "build"
  project                 = var.project_build
  auto_create_subnetworks = "false"
}

resource "google_compute_subnetwork" "build" {
  name          = "build"
  project       = var.project_build
  ip_cidr_range = "10.2.0.0/16"
  region        = var.region
  network       = google_compute_network.build.self_link
}

resource "google_compute_network_peering" "runner-build" {
  name         = "build"
  network      = data.google_compute_network.default.self_link
  peer_network = google_compute_network.build.self_link
}

resource "google_compute_network_peering" "build-runner" {
  name         = "build"
  network      = google_compute_network.build.self_link
  peer_network = data.google_compute_network.default.self_link
}

# Allow runner connect to build instances
# docker-machine expects a firewall rule called "docker-machines" and
# will create or modify it, if it does not match it's expectations
resource "google_compute_firewall" "build-allow-runner" {
  name    = "docker-machines"
  network = google_compute_network.build.name
  project = var.project_build

  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "tcp"
    ports    = ["22", "2376"]
  }

  source_ranges = ["10.128.0.0/9"]
}

# Allow build instances to connect to the runner
resource "google_compute_firewall" "build-allow-outgoing-runner" {
  name    = "build-allow-outgoing-runner"
  network = google_compute_network.build.name
  project = var.project_build

  direction = "EGRESS"

  allow {
    protocol = "all"
  }

  destination_ranges = ["10.128.0.0/9"]
}

# Allow build instances to http and https servers on the outside
resource "google_compute_firewall" "build-allow-outgoing-allow-extern" {
  name    = "build-allow-outgoing-extern"
  network = google_compute_network.build.name
  project = var.project_build

  direction = "EGRESS"

  allow {
    protocol = "tcp"
    ports    = ["80", "443"]
  }
}

# Deny any other outgoing traffic
resource "google_compute_firewall" "build-deny-outgoing" {
  name    = "build-deny-outgoing"
  network = google_compute_network.build.name
  project = var.project_build

  priority  = "65534"
  direction = "EGRESS"

  deny {
    protocol = "all"
  }
}

resource "google_compute_firewall" "runner-allow-build" {
  name    = "runner-allow-build"
  network = data.google_compute_network.default.name
  project = var.project

  allow {
    protocol = "tcp"
    ports    = ["9000"]
  }

  source_ranges           = [google_compute_subnetwork.build.ip_cidr_range]
  target_service_accounts = [google_service_account.runner.email]
}
