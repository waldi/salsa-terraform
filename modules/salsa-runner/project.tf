# Allow runner to create build instances
resource "google_project_iam_member" "runner-build-compute_instanceAdmin" {
  project = var.project_build
  role    = "roles/compute.instanceAdmin"
  member  = "serviceAccount:${google_service_account.runner.email}"
}

# Allow runner to attach build instances to networks
resource "google_project_iam_member" "runner-build-compute_networkUser" {
  project = var.project_build
  role    = "roles/compute.networkUser"
  member  = "serviceAccount:${google_service_account.runner.email}"
}

# Allow runner to attach build instances to service accounts
resource "google_project_iam_member" "runner-build-iam_serviceAccountUser" {
  project = var.project_build
  role    = "roles/iam.serviceAccountUser"
  member  = "serviceAccount:${google_service_account.runner.email}"
}

