resource "google_storage_bucket" "cache" {
  name          = "debian-salsa-${var.env}-runner-cache"
  project       = var.project
  location      = var.region
  storage_class = "REGIONAL"

  uniform_bucket_level_access = true

  lifecycle_rule {
    action {
      type = "Delete"
    }

    condition {
      age        = "14"
      with_state = "ANY"
    }
  }
}

resource "google_storage_bucket_iam_member" "runner" {
  bucket = google_storage_bucket.cache.name
  role   = "roles/storage.objectAdmin"
  member = "serviceAccount:${google_service_account.runner.email}"
}
