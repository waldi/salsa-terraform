resource "google_compute_instance_group" "vm" {
  name = "salsa-${var.env}-logs"
  zone = var.zone

  instances = [
    google_compute_instance.vm.self_link,
  ]

  named_port {
    name = "http"
    port = "5601"
  }

  named_port {
    name = "http-elasticsearch"
    port = "9280"
  }
}
