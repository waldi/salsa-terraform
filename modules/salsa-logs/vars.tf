variable "env" {
}

variable "project" {
}

variable "disk_size" {
}

variable "instance_type" {
}

variable "zone" {
}

variable "client_ips" {
  type = list(string)
}
