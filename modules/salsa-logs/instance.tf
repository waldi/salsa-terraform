resource "google_compute_instance" "vm" {
  name         = "salsa-${var.env}-logs"
  machine_type = var.instance_type
  zone         = var.zone

  allow_stopping_for_update = true

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-9"
      type  = "pd-ssd"
    }
  }

  attached_disk {
    source = google_compute_disk.data.self_link
  }

  network_interface {
    network = "default"

    access_config {
      // Ephemeral IP
    }
  }
}

resource "google_compute_disk" "data" {
  name = "salsa-${var.env}-logs-data"
  zone = var.zone
  type = "pd-ssd"
  size = var.disk_size
}
