terraform {
  backend "gcs" {
    bucket = "debian-salsa-prod-manage"
    prefix = "terraform-state"
  }
}
