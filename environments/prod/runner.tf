module "runner" {
  source = "../../modules/salsa-runner"

  env = "prod"

  project       = "debian-salsa-prod"
  project_build = "debian-salsa-prod-build"

  instance_type = "e2-micro"
  region        = "us-west1"
  zone          = "us-west1-c"
}
