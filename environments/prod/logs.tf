module "logs" {
  source = "../../modules/salsa-logs"

  env = "prod"

  project = "debian-salsa-prod"
  zone    = "us-west1-c"

  disk_size     = "100"
  instance_type = "e2-medium"

  client_ips = [
    # godard.debian.org
    "209.87.16.44",
  ]
}
