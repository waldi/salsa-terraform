module "lb" {
  source = "../../modules/lb"

  domain        = "salsa.debian.net"
  domain_gitlab = "salsa.debian.org"

  iap_brand = "projects/212751210318/brands/212751210318"

  addresses = [
    "34.95.66.130",
    "2600:1901:0:1d91::",
  ]

  group_dashboard = module.monitor.instance_group
  group_logs      = module.logs.instance_group
}
