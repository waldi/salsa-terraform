module "storage" {
  source = "../../modules/salsa-storage"

  env = "test"

  project = "debian-salsa-test"

  location      = "europe-west4"
  storage_class = "REGIONAL"

  origin          = "https://salsa-test.debian.net"
  origin_registry = "https://salsa-test-registry.debian.net"
}
