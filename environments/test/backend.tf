terraform {
  backend "gcs" {
    bucket = "debian-salsa-test-manage"
    prefix = "terraform-state"
  }
}
