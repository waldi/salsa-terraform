module "lb" {
  source = "../../modules/lb"

  domain        = "salsa-test.debian.net"
  domain_gitlab = "salsa-test.debian.net"

  iap_brand = "projects/668181963994/brands/668181963994"

  addresses = [
    "34.107.131.250",
    "2600:1901:0:c63e::",
  ]

  group_dashboard = module.monitor.instance_group
  group_logs      = module.logs.instance_group
}
