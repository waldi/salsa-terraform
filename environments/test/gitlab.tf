module "gitlab" {
  source = "../../modules/salsa-gitlab"

  env = "test"

  project = "debian-salsa-test"

  instance_type = "e2-medium"
  zone          = "europe-west4-c"
}
