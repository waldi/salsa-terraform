module "monitor" {
  source = "../../modules/salsa-monitor"

  env = "test"

  project = "debian-salsa-test"
  zone    = "europe-west4-c"

  disk_size     = "10"
  instance_type = "e2-micro"
}
