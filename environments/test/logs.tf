module "logs" {
  source = "../../modules/salsa-logs"

  env = "test"

  project = "debian-salsa-test"
  zone    = "europe-west4-c"

  disk_size     = "10"
  instance_type = "e2-small"

  client_ips = [
    module.gitlab.external_ip,
  ]
}
