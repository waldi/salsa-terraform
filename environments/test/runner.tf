module "runner" {
  source = "../../modules/salsa-runner"

  env = "test"

  project       = "debian-salsa-test"
  project_build = "debian-salsa-test-build"

  instance_type = "e2-micro"
  region        = "europe-west4"
  zone          = "europe-west4-c"
}
